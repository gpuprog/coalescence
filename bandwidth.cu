#include <stdio.h>

#define ROWS 2048
#define COLUMNS 2048
#define BLOCK_SIZE 16

// Copy kernel: access data in rows
__global__ void copyRow(float *out, float *in, const int nx, const int ny) {
	unsigned int ix = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int iy = blockDim.y * blockIdx.y + threadIdx.y;

	if (ix < nx && iy < ny) {
		out[iy * nx + ix] = in[iy * nx + ix];
	}
}

// Copy kernel: access data in columns
__global__ void copyCol(float *out, float *in, const int nx, const int ny) {
	unsigned int ix = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int iy = blockDim.y * blockIdx.y + threadIdx.y;

	if (ix < nx && iy < ny) {
		out[ix * ny + iy] = in[ix * ny + iy];
	}
}

// Copy kernel: access data in rows
__global__ void transposeRowToCol(float *out, float *in, const int nx, const int ny) {
	unsigned int ix = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int iy = blockDim.y * blockIdx.y + threadIdx.y;

	if (ix < nx && iy < ny) {
		out[ix * ny + iy] = in[iy * nx + ix];
	}
}

// Copy kernel: access data in columns
__global__ void transposeColToRow(float *out, float *in, const int nx, const int ny) {
	unsigned int ix = blockDim.x * blockIdx.x + threadIdx.x;
	unsigned int iy = blockDim.y * blockIdx.y + threadIdx.y;

	if (ix < nx && iy < ny) {
		out[iy * nx + ix] = in[ix * ny + iy];
	}
}

float throughput(int bytesRead, int bytesWritten, float timeElapsed) {
		return (bytesRead + bytesWritten) / timeElapsed / 10e9;
}

int main(int argc, char **argv) {
		cudaDeviceSetCacheConfig(cudaFuncCachePreferL1);

    int rows = argc > 1 ? atoi(argv[1]) : ROWS;
    int columns = argc > 2 ? atoi(argv[2]) : COLUMNS;
    int blockRows = argc > 3 ? atoi(argv[3]) : BLOCK_SIZE;
    int blockColumns = argc > 4 ? atoi(argv[4]) : blockRows;
    if (blockRows * blockColumns > 1024) {
        printf("block threads exceeding 1024: %d\n", blockRows * blockColumns);
        return 1;
    }
    printf("grid size = %dx%d", (int) ceil((double) rows / blockRows), (int) ceil((double) columns / blockColumns));
    printf(" (%d groups)\n", (int) ceil((double) rows / blockRows) * (int) ceil((double) columns / blockColumns));

    if (cudaSetDevice(0)) {
        printf("cannot set device 0\n");
        return 1;
    }

    float *a, *b, *x, *y;
    int i;
    int sizeInBytes = rows * columns * sizeof(float);
    a = (float*) malloc(sizeInBytes);
    b = (float*) malloc(sizeInBytes);

    for (i = 0; i < rows * columns; i++) {
        a[i] = i;
    }

    cudaMalloc((float**) &x, sizeInBytes);
    cudaMalloc((float**) &y, sizeInBytes);

    {
        cudaMemcpy(x, a, sizeInBytes, cudaMemcpyHostToDevice);

        dim3 dimBlock(blockColumns, blockRows, 1);
        dim3 dimGrid((int) ceil((double) columns / blockColumns), (int) ceil((double) rows / blockRows), 1);

        cudaEvent_t start, stop;
      	cudaEventCreate(&start);
      	cudaEventCreate(&stop);
      	float total_time = 0;  // total time of N tests
        cudaEventRecord(start);

        copyRow<<<dimGrid, dimBlock>>>(y, x, columns, rows);

        cudaEventRecord(stop);
    		cudaEventSynchronize(stop);
        float milliseconds = 0;
    		cudaEventElapsedTime(&milliseconds, start, stop);
        total_time += milliseconds;
    		printf("[copyRow] elapsed time: %.5f ms, tp: %.5f GBps\n", milliseconds, throughput(sizeInBytes, sizeInBytes, milliseconds / 1000));

        cudaMemcpy(b, y, sizeInBytes, cudaMemcpyDeviceToHost);
    }

    {
        cudaMemcpy(x, a, sizeInBytes, cudaMemcpyHostToDevice);

        dim3 dimBlock(blockColumns, blockRows, 1);
        dim3 dimGrid((int) ceil((double) columns / blockColumns), (int) ceil((double) rows / blockRows), 1);

        cudaEvent_t start, stop;
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        float total_time = 0;  // total time of N tests
        cudaEventRecord(start);

        copyCol<<<dimGrid, dimBlock>>>(y, x, columns, rows);

        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        float milliseconds = 0;
        cudaEventElapsedTime(&milliseconds, start, stop);
        total_time += milliseconds;
    		printf("[copyCol] elapsed time: %.5f ms, tp: %.5f GBps\n", milliseconds, throughput(sizeInBytes, sizeInBytes, milliseconds / 1000));

        cudaMemcpy(b, y, sizeInBytes, cudaMemcpyDeviceToHost);
    }

    {
        cudaMemcpy(x, a, sizeInBytes, cudaMemcpyHostToDevice);

        dim3 dimBlock(blockColumns, blockRows, 1);
        dim3 dimGrid((int) ceil((double) columns / blockColumns), (int) ceil((double) rows / blockRows), 1);

        cudaEvent_t start, stop;
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        float total_time = 0;  // total time of N tests
        cudaEventRecord(start);

        transposeRowToCol<<<dimGrid, dimBlock>>>(y, x, columns, rows);

        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        float milliseconds = 0;
        cudaEventElapsedTime(&milliseconds, start, stop);
        total_time += milliseconds;
    		printf("[transposeRowToCol] elapsed time: %.5f ms, tp: %.5f GBps\n", milliseconds, throughput(sizeInBytes, sizeInBytes, milliseconds / 1000));

        cudaMemcpy(b, y, sizeInBytes, cudaMemcpyDeviceToHost);
    }

    {
        cudaMemcpy(x, a, sizeInBytes, cudaMemcpyHostToDevice);

        dim3 dimBlock(blockColumns, blockRows, 1);
        dim3 dimGrid((int) ceil((double) columns / blockColumns), (int) ceil((double) rows / blockRows), 1);

        cudaEvent_t start, stop;
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
        float total_time = 0;  // total time of N tests
        cudaEventRecord(start);

        transposeColToRow<<<dimGrid, dimBlock>>>(y, x, columns, rows);

        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        float milliseconds = 0;
        cudaEventElapsedTime(&milliseconds, start, stop);
        total_time += milliseconds;
    		printf("[transposeColToRow] elapsed time: %.5f ms, tp: %.5f GBps\n", milliseconds, throughput(sizeInBytes, sizeInBytes, milliseconds / 1000));

        cudaMemcpy(b, y, sizeInBytes, cudaMemcpyDeviceToHost);
    }
}
